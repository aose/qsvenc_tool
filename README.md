QSVEnc Tool
====

[QSVEnc](https://github.com/rigaya/QSVEnc)でいい感じにエンコードするスクリプト
- フォルダ単位でまとめてエンコード
- エンコードが完了した元ファイルを削除

# 使い方
PowerShellから直接実行するか、`run.bat`を使ってGUIで実行する。

#### CLI
```
> .\encode.ps1 [-DeleteOriginalFile] -TargetDir \path\to\dir
```
※`TargetDir`をつけなかった場合、GUI同様にダイアログで対象フォルダを選択できる。

#### GUI
`run.bat`を実行する


# 設定方法
`config.json`にまとめて記述する。
```json
{
    "QSVEncDir": "C:\\Path\\To\\QSVEncC\\x64",
    "QSVEncFile": "QSVEncC64.exe",
    "EncodeOptions": [
        "--audio-copy",
        "--tff",
        "--vpp-deinterlace bob",
        "--icq 24",
        "--quality 1",
        "--codec hevc",
        "--profile main10",
        "--output-depth 10"
    ]
}
```

- QSVEncDir
    - QSVEncCのフォルダを指定
    - JSONなので`\`はエスケープが必要。要注意
- QSVEncFile
    - 実行ファイルの名前を指定。ここ変えることはないと思う
- EncodeOptions
    - QSVEncCに渡すオプションを記述
    - `-i inputfile`と`-o outputfile`はencode.ps1内にあるので記載不要
    - 内部で結合するので1行で書いても複数行に分けてもOK