Param(
    [switch]$DeleteOriginalFile,    # 元ファイルを削除する
    [string]$TargetDir              # エンコード対象フォルダ
)

# config読み込み
$scriptDir = $PSScriptRoot
$configPath = Join-Path $scriptDir "config.json"
$config = Get-Content $configPath | ConvertFrom-Json

# 対象フォルダ、削除確認
Add-Type -AssemblyName System.Windows.Forms
If ($TargetDir.Length -eq 0){

    $FolderBrowser = New-Object System.Windows.Forms.FolderBrowserDialog -Property @{ 
        RootFolder = "MyComputer"
        Description = '対象のフォルダを選択してください。'
    }
    
    if($FolderBrowser.ShowDialog() -eq [System.Windows.Forms.DialogResult]::OK){
        $TargetDir = $FolderBrowser.SelectedPath
    }else{
        [System.Windows.Forms.MessageBox]::Show('フォルダが選択されませんでした。終了します。')
        exit 0
    }

    $DeleteOriginalFile = ([System.Windows.Forms.MessageBox]::Show("エンコード後、元のファイルを削除しますか？","確認","YesNo","Question","Button2") -eq "Yes")
}else{
    $TargetDir = Convert-Path $TargetDir
}

If (Test-Path -PathType Container -Path $TargetDir) {
    Write-Host "エンコード対象フォルダ：" $TargetDir
}else{
    Write-Host "指定されたフォルダが見つかりませんでした。" $TargetDir
}
if ($DeleteOriginalFile){
    Write-Host "エンコードが完了したファイル：削除する"
} else {
    Write-Host "エンコードが完了したファイル：削除しない"
}

Get-ChildItem -File -Recurse -Path $TargetDir '*.ts' |
ForEach-Object -Process {
    $inputFilePath = $_.FullName
    $outputFilePath = Join-Path `
        (Split-Path -Parent $inputFilePath) `
        (([System.IO.Path]::GetFileNameWithoutExtension($inputFilePath)) + ".mp4")
    #Write-Host $outputFilePath

    $options = @()
    $config.EncodeOptions | ForEach-Object -Process {$options += $_}
    $options += "-i ""${inputFilePath}"""
    $options += "-o ""${outputFilePath}"""

    $result = Start-Process `
        -WorkingDirectory $config.QSVEncDir `
        -FilePath         $config.QSVEncFile `
        -NoNewWindow `
        -Wait `
        -PassThru `
        -ArgumentList $options
    
    If ($result.ExitCode -eq 0) {
        If ($DeleteOriginalFile) {
            Remove-Item -Force -Path $inputFilePath
        }
    }else{
        Write-Host "エンコードに失敗しました。終了します。"
        exit 1
    }
}